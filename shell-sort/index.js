"use strict";
// From https://www.geeksforgeeks.org/shellsort/
function init() {
  // Just for encapsulation from other files
  // Dummy data
  const arr1 = [1, 6, 2, 8, 3];
  const arr2 = [12, -1, 0, 3, 3];
  const arr3 = [-8, 6, 0, -1, 3, -10];
  const arr4 = [5];
  const arr5 = [];
  const arr6 = [6, 1, 2, 2, 2, 1, 5, -14];
  const arr7 = [10, 55, -5, 34, 7, 22, 19];

  const shellSort = (arr) => {
    let counter = 1;

    console.log(`Before shell sorting: ${arr}`);
    let n = arr.length;

    for (let gap = Math.floor(n / 2); gap > 0; gap = Math.floor(gap / 2)) {
      console.log(`gap is: ${gap}`);
      for (let i = gap; i < n; i++) {
        const temp = arr[i];
        let j;
        for (j = i; j >= gap && arr[j - gap] > temp; j -= gap)
          arr[j] = arr[j - gap];
        arr[j] = temp;
        counter++;
      }
    }

    console.log(`After shell sorting: ${arr}`);
    console.log(`Counter After shell sorting: ${counter}`);
  };

  shellSort(arr1);
  shellSort(arr2);
  shellSort(arr3);
  shellSort(arr4);
  shellSort(arr5);
  shellSort(arr6);
  shellSort(arr7);
}
init();
